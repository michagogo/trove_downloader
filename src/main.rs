extern crate structopt;
extern crate reqwest;

use std::{io, fs, fs::File, collections::BTreeMap};
use serde::{Deserialize, Serialize};
use serde_json;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(about, author)]
enum Opt {
    /// Update the program. its basic in that it downloads a fresh copy from the repo, dosnt check version or anything yet
    Update {
        /// Choose platform to update: windows, linux. It auto checks itself but can be overwritten by passing the OS here.
        #[structopt(short, long, required = false, default_value = "")]
        platform: String,
    },
    /// How the program runs normally
    Run {
        /// Session key
        /// Optional after the first run.
        #[structopt(short, long, required = false, default_value = "")]
        key : String,

        /// Save Location
        /// Defaults to same folder as executable if left blank.
        /// Optional after the first run.
        #[structopt(short, long, required = false, default_value = "")]
        location: String,

        /// Platform
        /// Possible Values: all, windows, mac, linux, asmjs, unitywasm.
        /// Defaults to windows if left blank.
        /// Optional after the first run.
        #[structopt(short, long, required = false, default_value = "")]
        platform: String,

        /// Folders
        /// Decides the folder layout
        /// Original: Puts files into folders based off of what humble themselves suggest, ./windows/Frozenbyte/trinegame_v1234.zip
        /// Game: Puts files in a folder of the games name such as ./windows/Trine/trinegame_v1234.zip
        /// Reverse: Puts files in a game folder first then sunbdevides into platform. ./Trine/windows/trinegame_v1234.zip
        /// Defaults to Game.
        #[structopt(short, long, required = false, default_value = "")]
        folders: String,
    }
}

fn main() {
    let opt:Opt = Opt::from_args();

    // decides what to do
    match opt {
        Opt::Update {platform} => {
            // calls the update function
            get_update(platform);
        },
        // Sets up Parameters then runs
        Opt::Run{key,location,platform, folders} => {
            let config = get_config(key, location, platform, folders);

            // get fresh trove data file
            // this is to allow it to always get the fresh version
            get_trove_data(&config.api);

            // this is where all the magic happens
            process_list(config);
        }
    }
}

fn get_update(platform_override:String){
    // for when I cna do version checking
    //const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");
    //println!("{}", VERSION.unwrap_or("unknown"));
    // https://gitlab.com/silver_rust/trove_downloader/raw/master/Cargo.toml?inline=false

    let mut platform;
    if cfg!(windows) {
        platform = "windows".to_string();
    } else if cfg!(unix) {
        // assuming a tad here
        platform = "linux".to_string();
    } else {
        platform = "unknown".to_string();
    }

    if platform_override != "" {
        platform = platform_override;
    }


    if platform == "windows" {
        let download_url = "https://gitlab.com/silver_rust/trove_downloader/-/jobs/artifacts/master/raw/target/x86_64-pc-windows-gnu/release/trove_downloader.exe?job=x86_64-pc-windows-gnu";

        get_file(&download_url, ".", "trove_downloader.exe.new");
        fs::rename("trove_downloader.exe", "trove_downloader.exe.old").expect("failed to rename running file to old file");
        fs::rename("trove_downloader.exe.new", "trove_downloader.exe").expect("failed to rename new file to running file");

        // this will leave a "trove_downloader.exe.old" but that will get overwritten next time
    }

    if platform == "linux" {
        let download_url = "https://gitlab.com/silver_rust/trove_downloader/-/jobs/artifacts/master/raw/target/x86_64-unknown-linux-musl/release/trove_downloader?job=x86_64-unknown-linux-musl";

        get_file(&download_url, ".", "trove_downloader.new");
        fs::remove_file("trove_downloader").expect("failed to delete running file");
        fs::rename("trove_downloader.new", "trove_downloader").expect("failed to rename new file to running file");
        // TODO set permissions for trove_downloader to executable
    }

}

fn get_trove_data(api:&String){
    let mut new_json:Vec<ItemData> = Vec::new();

    let mut n:i32 = 0;
    let mut done = false;

    let auth = api.as_str();
    let client = reqwest::Client::new();
    while !done {
        let mut url:String = "https://www.humblebundle.com/api/v1/trove/chunk?index=".to_string();
        url.push_str(&n.to_string());

        let mut resp = client.get(&url)
            .header("cookie", auth)
            .send()
            .expect("Something went wrong getting the download link");

        let response = resp.text().expect("Something went wrong converting to a string");
        let json:Vec<ItemData> = serde_json::from_str(&response).expect("failed to convert to json");


        if json.len() == 0 {
            done = true;
        }

        for x in json {
            new_json.push(x);
        }
        n += 1;
    }
    println!("Trove has {} items in it",new_json.len());

    new_json.sort_by(|a, b| a.human_name.cmp(&b.human_name));
    let new_json_string = serde_json::to_string_pretty(&new_json).expect("failed to convert to string");
    let dir = "config/";
    let file = "config/trove.json";

    fs::create_dir_all(dir).expect("Something went wrong creating the directory");
    fs::write(file, new_json_string).expect("failed to create file");
}

#[derive(Serialize, Deserialize)]
struct Config {
    api: String,
    platform: String,
    location: String,
    // this is due to it not being in the config originally
    #[serde(default = "default_blank")]
    folders: String
}

fn get_config(key:String,location:String,platform:String, folders:String) -> Config{
    let config_path = "config/config.json";

    // start config as empty
    let mut config:Config = Config {
        api: "".to_string(),
        platform: "".to_string(),
        location: "".to_string(),
        folders: "".to_string()
    };

    // check if config file exists
    if fs::metadata(&config_path).is_ok(){
        // if it exists
        // read from file

        println!("Using arguments from config file");
        let data = fs::read_to_string(&config_path).expect("Something went wrong reading the config file");
        let config_file:Config = serde_json::from_str(&data).expect("Something went wrong converting to json 160");

        // set the values based on if they are present or not
        if key == "" {
            config.api =  config_file.api;
        } else {
            println!("Using new key");
            config.api = "_simpleauth_sess=\"".to_string() + &key + "\"";
        }

        if platform == "" {
            config.platform =  config_file.platform;
        } else {
            println!("Using new platform");
            config.platform = platform;
        }

        if location == "" {
            config.location =  config_file.location;
        } else {
            println!("Using new location");
            config.location = location;
        }

        if folders == "" {
            config.folders =  config_file.folders;
        } else {
            println!("Using new folder format");
            config.folders = folders;
        }
    } else {
        // if it dosent use the passed config values
        println!("Creating new config file");

        if key != "" {
            config.api = "_simpleauth_sess=\"".to_string() + &key + "\"";
        }
        config.platform = platform;
        config.location = location;
        config.folders = folders;
    }

    // final check
    // cant do anything without the session key
    if config.api == "" {
        println!("session key is not set");
        panic!();
    }
    // if blank set default platform to windows
    if config.platform == "" {
        config.platform = "windows".to_string();
    }
    // defaults to current folder if blank
    if config.location == "" {
        config.location = ".".to_string();
    }
    // defaults to current folder if blank
    if config.folders == "" {
        config.folders = "Game".to_string();
    }

    let data = serde_json::to_string_pretty(&config).expect("Something went wrong converting json to string");
    fs::create_dir_all("config/").expect("Something went wrong creating the directory");
    fs::write(&config_path, data).expect("Unable to write file");
    config
}

// my main function for downloading files
fn get_file(url: &str, dir: &str , file: &str){
    let mut resp = reqwest::get(url).expect("request failed");

    // breaks it off if anythign goes wonky
    if !resp.status().is_success(){
        println!("Error downloading: {} . Got {}",url, resp.status());
        panic!();
    }

    fs::create_dir_all(dir).expect("Something went wrong creating the directory");
    let mut out = File::create(file).expect("failed to create file");
    io::copy(&mut resp, &mut out).expect("failed to copy content");
}

#[derive(Serialize, Deserialize, Debug)]
struct ItemData {
    downloads: Downloads,
    #[serde(rename = "human-name")]
    human_name: String,
    image: String
}

#[derive(Serialize, Deserialize, Debug)]
struct DisplayItemData {
    downloads: Downloads
}

#[serde(rename_all = "camelCase")]
#[derive(Serialize, Deserialize, Debug)]
struct Downloads {
    windows: Option<Download>,
    mac: Option<Download>,
    linux: Option<Download>,
    asmjs: Option<Download>,
    unitywasm: Option<Download>
}

#[derive(Serialize, Deserialize, Debug)]
struct Download {
    url: Url,
    md5: String,
    machine_name:String
}

#[serde(rename_all = "camelCase")]
#[derive(Serialize, Deserialize, Debug)]
struct Url {
    web: String
}

#[serde(rename_all = "camelCase")]
#[derive(Serialize, Deserialize, Debug)]
struct Progress {
    name: String,
    #[serde(default = "default_progress")]
    image: String,
    #[serde(default = "default_progress")]
    windows: String,
    #[serde(default = "default_progress")]
    mac: String,
    #[serde(default = "default_progress")]
    linux: String,
    #[serde(default = "default_progress")]
    asmjs: String,
    #[serde(default = "default_progress")]
    unitywasm: String
}

fn default_progress() -> String {
    "none".to_string()
}

fn default_blank() -> String {
    "".to_string()
}

// windows, mac, linux, asmjs, unitywasm
fn process_list(config:Config) {
    let trove_data = fs::read_to_string("./config/trove.json").expect("Something went wrong reading the config file");
    let trove:Vec<ItemData> = serde_json::from_str(&trove_data).expect("Something went wrong converting to json 128");

    let platforms_supported = vec!["asmjs".to_string(), "linux".to_string(), "mac".to_string(), "unitywasm".to_string(), "windows".to_string()];
    if &config.platform == "all" {
        for position  in 0..platforms_supported.len() {
            println!("{} - Starting",platforms_supported[position]);
            process_download(&config, &trove, &platforms_supported[position]);
        }
    } else if &config.platform.contains(",") == &true {
        // multiple different

        let temp = &config.platform.to_string();
        let platforms = temp.split(",");

        for s in platforms {
            println!("{} - Starting", s);

            if platforms_supported.contains(&s.to_string()) {
                process_download(&config, &trove, &s.to_string());
            } else {
                println!("{} - Not a valid platform", s);
            }
        }
    } else {
        println!("{} - Starting", &config.platform);

        if platforms_supported.contains(&config.platform.to_string()) {
            process_download(&config, &trove, &config.platform);
        } else {
            println!("{} - Not a valid platform", &config.platform);
        }

    }
}

fn get_progress_list_as_json(downloads:&Downloads, name:&String, image:&String) -> BTreeMap<String, Progress>{
    let path = "./config/progress.json";
    let progress = fs::File::open(&path);
    match progress {
        Ok(_file) => {},
        Err(_error) => {
            fs::write(&path, "{}").expect("Unable to write file");
        }
    };
    let progress_data = fs::read_to_string(&path).expect("Something went wrong reading the config file");
    let mut progress:BTreeMap<String, Progress> = serde_json::from_str(&progress_data).expect("Something went wrong converting to json 194");

    let progress = match progress.get(name) {
        Some(_x) => progress,
        None => {
            let windows_exists = match &downloads.windows{
                Some(_x) => "",
                None => "none"
            };
            let mac_exists = match &downloads.mac{
                Some(_x) => "",
                None => "none"
            };
            let linux_exists = match &downloads.linux{
                Some(_x) => "",
                None => "none"
            };
            let asmjs_exists = match &downloads.asmjs{
                Some(_x) => "",
                None => "none"
            };
            let unitywasm_exists = match &downloads.unitywasm{
                Some(_x) => "",
                None => "none"
            };

            progress.insert(
                name.to_string(),
                Progress{
                    name: name.to_string(),
                    image: image.to_string(),
                    windows: windows_exists.to_string(),
                    mac: mac_exists.to_string(),
                    linux: linux_exists.to_string(),
                    asmjs: asmjs_exists.to_string(),
                    unitywasm: unitywasm_exists.to_string()
                }
            );
            progress
        }
    };

    progress
}


#[derive(Serialize, Deserialize, Debug)]
struct DownloadUrl {
    signed_url: String
}

fn get_download_links(api:&String,machine_name:&String, filename: &String) -> String{
    let params = [("machine_name", machine_name), ("filename", filename)];
    let client = reqwest::Client::new();
    let auth = api.as_str();
    let mut resp = client.post("https://www.humblebundle.com/api/v1/user/download/sign")
        .form(&params)
        .header("cookie", auth)
        .send()
        .expect("Something went wrong getting the download link");

    let response = &resp.text().expect("Something went wrong converting to a string");
    let download_url:DownloadUrl = serde_json::from_str(&response).expect("Something went wrong converting download link to json");
    download_url.signed_url
}

fn remove_slashes(s: &String) -> (&str,&str ){
    let bytes = s.as_bytes();
    let mut cutoff = 0;
    for (i, &item) in bytes.iter().enumerate() {
        if item == b'/' {
            cutoff = i;
        }
    }
    let offset = &s[..cutoff];
    let file = &s[cutoff..];
    (offset,file)
}

fn save_progress(mut progress:BTreeMap<String, Progress>, name:&String, platform: &String, download_data: &Download, image:&String){
    let windows;
    let mac;
    let linux;
    let asmjs;
    let unitywasm;

    match progress.get(name){
        Some(x)=>{
            if platform == "windows" {
                windows = download_data.md5.to_string();
                mac = x.mac.to_string();
                linux = x.linux.to_string();
                asmjs = x.asmjs.to_string();
                unitywasm = x.unitywasm.to_string();
            } else if platform == "mac" {
                windows= x.windows.to_string();
                mac=download_data.md5.to_string();
                linux= x.linux.to_string();
                asmjs = x.asmjs.to_string();
                unitywasm = x.unitywasm.to_string();
            } else if platform == "linux" {
                windows= x.windows.to_string();
                mac= x.mac.to_string();
                linux= download_data.md5.to_string();
                asmjs = x.asmjs.to_string();
                unitywasm = x.unitywasm.to_string();
            } else if platform == "asmjs" {
                windows= x.windows.to_string();
                mac= x.mac.to_string();
                linux= x.linux.to_string();
                asmjs = download_data.md5.to_string();
                unitywasm = x.unitywasm.to_string();
            } else if platform == "unitywasm" {
                windows= x.windows.to_string();
                mac= x.mac.to_string();
                linux= x.linux.to_string();
                asmjs = x.asmjs.to_string();
                unitywasm = download_data.md5.to_string();
            } else {
                panic!()
            };
        },
        None=>panic!()
    };

    let inserting = Progress{
        name: name.to_string(),
        image: image.to_string(),
        windows: windows.to_string(),
        mac: mac.to_string(),
        linux: linux.to_string(),
        asmjs: asmjs.to_string(),
        unitywasm: unitywasm.to_string()
    };
    progress.insert(name.to_string(),inserting);
    let j = serde_json::to_string_pretty(&progress).expect("Unable to convert to string");
    fs::write("./config/progress.json", j).expect("Unable to write progress file");
}

fn process_download(config:&Config, trove:&Vec<ItemData>, platform:&String){
    for item in 0..trove.len() {
        let name = &trove[item].human_name;
        let image = &trove[item].image;
        let name_clean = &name.replace(|c: char| !(c.is_alphanumeric() || c == ' '), "").replace("  ", " ").replace("   ", " ");

        // gets the progress list, if the item is new it creates an entry
        let progress = get_progress_list_as_json(&trove[item].downloads, &name, &image);

        let download_data;
        // skips if the platform isnt available
        // sets up the paths if it is available
        match progress.get(name){
            Some(x) => {
                if platform == "windows" {
                    if x.windows == "none" {continue};
                    download_data = match &trove[item].downloads.windows{ Some(x) => {x}, None => panic!() };
                    if x.windows == download_data.md5.to_string(){continue};
                } else if platform == "mac" {
                    if x.mac == "none" {continue};
                    download_data = match &trove[item].downloads.mac{ Some(x) => {x}, None => panic!() };
                    if x.mac == download_data.md5.to_string(){continue};
                } else if platform == "linux" {
                    if x.linux == "none" {continue};
                    download_data = match &trove[item].downloads.linux{ Some(x) => {x}, None => panic!() };
                    if x.linux == download_data.md5.to_string(){continue};
                } else if platform == "asmjs" {
                    if x.asmjs == "none" {continue};
                    download_data = match &trove[item].downloads.asmjs{ Some(x) => {x}, None => panic!() };
                    if x.asmjs == download_data.md5.to_string(){continue};
                } else if platform == "unitywasm" {
                    if x.unitywasm == "none" {continue};
                    download_data = match &trove[item].downloads.unitywasm{ Some(x) => {x}, None => panic!() };
                    if x.unitywasm == download_data.md5.to_string(){continue};
                } else {
                    //panic!()
                    println!("Platform: {} is not supported", platform);
                    continue
                }
            },
            None => {continue}
        };

        // get the download link from Humble Bundle's API
        let download_url = get_download_links(&config.api,&download_data.machine_name,&download_data.url.web);

        // some files are stored in folders, deal with this
        let (offset, file_name) = remove_slashes(&download_data.url.web);

        // check if first character is uppercase
        // if so then it could be a preset
        let custom;
        if config.folders.chars().next().unwrap().is_uppercase(){
            if config.folders == "Original" {
                // old format - is based on the data from humble themselves, often ./publisher/file.ext
                custom = "platform,offset";
            }
            else if config.folders == "Game" {
                // Human_Name/file_name
                custom = "platform,name";
            }
            else if config.folders == "Reverse" {
                // reverses the layout  into games then platforms
                custom = "name,platform";
            }
            else {
                custom = &config.folders;
            }
        }
        // if not treat it as custom
        else{
            custom = &config.folders;
        }

        // set base
        let mut directory= config.location.to_owned() + "/";

        for s in custom.split(",") {
            let sub_item = match s {
                "platform" => platform,
                "offset" => offset,
                "name" => name_clean,
                // catch all non valid options
                _ => ""
            };
            if sub_item != "" { directory = directory + sub_item + "/"; }
        }

        let file = directory.to_owned() + file_name;

        // downloading the actual file and saving it to the right location
        println!("Downloading {} file {}/{} {}",platform,item+1,trove.len(), &file);
        get_file(&download_url, &directory, &file);

        // now mark the progress as done for that platform and save file
        save_progress(progress, name, &platform, &download_data, image);
    }
}
